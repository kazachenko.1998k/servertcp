#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <list>
#include <thread>
#include <iostream>
#include <c++/8/sstream>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "sql.cpp"

using namespace std;

int listener;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
list<string> threadsID;
list<int> socks;
list<thread> threads;


string toStr(thread::id id) {
    stringstream ss;
    ss << id;
    return ss.str();
}

string listToString() {
    string listThreads = "list threadsID: ";
    for (const string &v : threadsID) {
        listThreads += v;
        listThreads += " ";
    }
    listThreads += "\n";
    for (thread &v : threads) {
        listThreads += toStr(v.get_id());
        listThreads += " ";
    }
    listThreads += "\n";

    return listThreads;
}

list<string> split(const string &s, char delimiter) {
    list<string> tokens;
    string token;
    istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

void exit() {
    shutdown(listener, 2);
    close(listener);
}

bool containsThread(const string &id) {
    for (const string &elem : threadsID) {
        if (strcmp(elem.c_str(), id.c_str()) == 0) {
            return true;
        }
    }
    return false;
}

void kill(const string &id, string &pString) {
    if (containsThread(id)) {
        threadsID.remove(id);
        pString = "killed ";
        pString += id;
    } else {
        pString = "Can not kill this";
    }
    pString += "\n";
}

void commandLine() {
    while (true) {
        string message;
        getline(cin, message);
        list<string> listSplit = split(message, ' ');
        if (strcmp(message.c_str(), string("list").c_str()) == 0) {
            cout << listToString();
        } else if (strcmp(message.c_str(), string("exit").c_str()) == 0) {
            cout << "Exit";
            exit();
            break;
        } else {
            listSplit = split(message, ' ');
            if (strcmp(listSplit.front().c_str(), string("kill").c_str()) == 0) {
                listSplit.pop_front();
                kill(listSplit.front(), message);
                cout << message;
            }
        }
    }
}


// чтение из сокета заданного кол-ва байт
int readn(int sockfd, void *dst, size_t len) {
    int total_number_read = 0;
    int local_number_read = 0;
    void *tmp_buffer = malloc(len);

    while (len > 0) {
        bzero(tmp_buffer, len + total_number_read);
        local_number_read = read(sockfd, tmp_buffer, len);
        memcpy(dst + total_number_read, tmp_buffer, local_number_read);

        if (local_number_read == 0) {
            free(tmp_buffer);
            return total_number_read;
        }

        if (local_number_read < 0) {
            free(tmp_buffer);
            return -1;
        }

        total_number_read += local_number_read;
        len -= local_number_read;
    }

    free(tmp_buffer);
    return total_number_read;
}

string listCommand() {
    string message;
    message += "exit - выход\n";
    message += "help - вывод подсказки с действиями\n";
    message += "signUp xxx- добавить нового пользователя, где ххх это имя\n";
    message += "signIn xxx- войти с учетной записи пользователя, где ххх это имя\n";
    message += "listUsers - вывести список всех действующих покупателей\n";
    message += "listProducts - вывести список всех действующих закупок\n";
    message += "listMy - вывести список действующих моих закупок\n";
    message += "truncate - очистить список связей пользователей и покупок. Только от имеени администратора. \n";
    message += "buy yyy zzz - купить продукт. yyy - id продукта, zzz - кол-во продукта\n";
    message += "insert xxx yyy zzz - добавить новый продукт, где xxx - название продукта, ууу - цена за единицу товара, zzz - кол-во продукта\n";
    return message;
}

void fun(int sock) {
    socklen_t len;
    struct sockaddr_storage addr{};
    char ipstr[INET6_ADDRSTRLEN];
    int port;
    len = sizeof addr;
    getpeername(sock, (struct sockaddr *) &addr, &len);
    auto *s = (struct sockaddr_in *) &addr;
    port = ntohs(s->sin_port);
    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
    string hello = "Peer IP address: ";
    hello += ipstr;
    hello += ":";
    hello += to_string(port);
    hello += "\n";
    hello += listCommand();
    send(sock, hello.c_str(), 1024, 0);

    string name;

    while (true) {
        string message;
        char buf[1024];
        int bytes_read;

        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        bytes_read = readn(sock, buf, 1024);
        if (bytes_read <= 0) {
            break;
        }
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        cout << buf << "\n";
        if (strcmp(buf, string("help").c_str()) == 0) {
            message = listCommand();
        } else if (strcmp(buf, string("exit").c_str()) == 0) {
            message = "Good bye my friend";
        } else {
            list<string> listSplit = split(buf, ' ');
            string com = listSplit.front();
            if (com == "signUp") {
                if (listSplit.size() < 2) {
                    message = listCommand();
                } else {
                    listSplit.pop_front();
                    name = listSplit.front();
                    message = insertUser(name);
                }
            } else if (com == "signIn") {
                if (listSplit.size() < 2) {
                    message = listCommand();
                } else {
                    listSplit.pop_front();
                    name = listSplit.front();
                    if (name.length() > 0) {
                        message = "Hello ";
                        message += name;;
                    } else {
                        message = "Sorry, but user with name ";
                        message += name;
                        message += " could'n find...";
                    }
                }
            } else if (name.empty()) {
                message = listCommand();
            } else if (strcmp(buf, string("listUsers").c_str()) == 0) {
                message = selectAllUsers();
            } else if (strcmp(buf, string("listProducts").c_str()) == 0) {
                message = selectAllProducts();
            } else if (strcmp(buf, string("listMy").c_str()) == 0) {
                message = selectMyOrder(name);
            } else if (strcmp(buf, string("truncate").c_str()) == 0) {
                if (name == "admin") {
                    message = truncate();
                } else {
                    message = "Only admin can truncate db";
                }
            } else if (listSplit.front() == "insert") {
                list<string> listSplitText = split(buf, ' ');
                if (name == "admin") {
                    if (listSplitText.size() < 4) {
                        message = listCommand();
                    } else {
                        listSplitText.pop_front();
                        string nameT = listSplitText.front();
                        listSplitText.pop_front();
                        string priceT = listSplitText.front();
                        listSplitText.pop_front();
                        string count_all = listSplitText.front();
                        message = insertProducts(nameT, priceT, count_all);
                    }
                } else {
                    message = "Only admin can add products";
                }
            } else if (listSplit.front() == "buy") {
                list<string> listSplitText = split(buf, ' ');
                if (listSplitText.size() < 3) {
                    message = listCommand();
                } else {
                    listSplitText.pop_front();
                    string idProduct = listSplitText.front();
                    listSplitText.pop_front();
                    string count_all = listSplitText.front();
                    message = buy(name, idProduct, count_all);
                    string last = productsIsEmpty();
                    if (atoi(last.c_str()) <= 0) {
                        printf("PRODUCTS EMPTY\n");
                        string salary = productsSalary();
                        printf("SALARY = %s\n", salary.c_str());
                    } else {
                        printf("PRODUCTS LAST %s\n", last.c_str());
                    }
                }
            } else {
                message = listCommand();
            }
        }
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        send(sock, message.c_str(), 1024, 0);
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        if (strcmp(buf, string("exit").c_str()) == 0) {
            break;
        }
    }
    shutdown(sock, 2);
    close(sock);
    pthread_mutex_lock(&mutex);
    threadsID.remove(toStr(this_thread::get_id()));
    socks.remove(sock);
    pthread_mutex_unlock(&mutex);
}

int main(int argc, char *argv[]) {
    connect();
    int sock;
    struct sockaddr_in addr{};
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener < 0) {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[1]));
    addr.sin_addr.s_addr = INADDR_ANY;
    const int on = 1;
    setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (bind(listener, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(2);
    }

    listen(listener, 1);
    thread cl(commandLine);
    while (true) {
        sock = accept(listener, nullptr, nullptr);
        if (sock < 0) {
            perror("accept");
            break;
        }
        pthread_mutex_lock(&mutex);
        threads.emplace_back([&] { fun(sock); });
        socks.emplace_back(sock);
        threadsID.emplace_back(toStr(threads.back().get_id()));
        pthread_mutex_unlock(&mutex);
    }

    cl.join();

    pthread_mutex_lock(&mutex);
    for (int &socked : socks) {
        shutdown(socked, 2);
        close(socked);
    }
    pthread_mutex_unlock(&mutex);

    for (thread &thread : threads) {
        if (containsThread(toStr(thread.get_id()))) {
            thread.join();
        }
    }

    PQfinish(conn);
    return 0;

}