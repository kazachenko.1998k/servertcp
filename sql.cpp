//
// Created by konstantin on 15.11.2019.
//
#include <cstdlib>
#include <libpq-fe.h>

using namespace std;

#define UNUSED(x) (void)(x)


static PGconn *conn = nullptr;
static PGresult *res = nullptr;

static string terminate(int code) {
    string result = string("allOk");
    if (code != 0) {
        fprintf(stderr, "%s\n", PQerrorMessage(conn));
        result = string(PQerrorMessage(conn));
    }
    if (res != nullptr)
        PQclear(res);

    if (conn != nullptr)
        PQfinish(conn);
    return result;
}

static void clearRes() {
    PQclear(res);
    res = nullptr;
}

static void processNotice(void *arg, const char *message) {
    UNUSED(arg);
    UNUSED(message);

    // do nothing
}

int dropAllTable() {

    res = PQexec(conn, "DROP TABLE IF EXISTS products CASCADE;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }

    res = PQexec(conn, "DROP TABLE IF EXISTS users CASCADE;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }


    return 0;
}

bool connect() {
    int libpq_ver = PQlibVersion();
    printf("Version of libpq: %d\n", libpq_ver);

    conn = PQconnectdb("user=postgres password=qwe host=127.0.0.1 dbname=maria");
    if (PQstatus(conn) != CONNECTION_OK)
        return (false);

    PQsetNoticeProcessor(conn, processNotice, nullptr);

    int server_ver = PQserverVersion(conn);
    char *user = PQuser(conn);
    char *db_name = PQdb(conn);
    printf("Server version: %i\n", server_ver);
    printf("User: %s\n", user);
    printf("Database name: %s\n", db_name);

//    dropAllTable();


    res = PQexec(conn, "CREATE TABLE IF NOT EXISTS users "
                       "(id SERIAL PRIMARY KEY, name VARCHAR(64) UNIQUE);");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (false);
    }
    clearRes();

    res = PQexec(conn,
                 "CREATE TABLE IF NOT EXISTS products "
                 "(id SERIAL PRIMARY KEY, "
                 "name VARCHAR(128), "
                 "price INT NOT NULL, "
                 "count_all INT NOT NULL,"
                 "check (count_all >=0 ));");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (false);
    }
    clearRes();
    res = PQexec(conn,
                 "CREATE TABLE IF NOT EXISTS products_users "
                 "(id SERIAL PRIMARY KEY, "
                 "user_id INT NOT NULL REFERENCES users(id), "
                 "product_id INT NOT NULL REFERENCES products(id), "
                 "count_buy INT NOT NULL);");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (false);
    }
    clearRes();

    return true;
}

string selectAllUsers() {
    string result;
    res = PQexec(conn, "SELECT * "
                       "FROM users;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectAllProducts() {
    string result;
    res = PQexec(conn, "SELECT * "
                       "FROM products;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);


    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *price = PQgetvalue(res, i, 2);
        char *count_all = PQgetvalue(res, i, 3);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Price: ";
        result += price;
        result += " count all: ";
        result += count_all;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}


string buy(const string &name_user,
           const string &id_product,
           const string &count) {
    const char *query_check =
            "UPDATE products SET count_all = count_all - $2 WHERE id = $1;";
    const char *params_check[2];
    params_check[0] = id_product.c_str();
    params_check[1] = count.c_str();
    res = PQexecParams(conn, query_check, 2, nullptr, params_check,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();

    const char *query =
            "INSERT INTO products_users (user_id , product_id, count_buy) "
            " VALUES ((SELECT id FROM users WHERE name = $1), $2, $3);";
    const char *params[3];
    params[0] = name_user.c_str();
    params[1] = id_product.c_str();
    params[2] = count.c_str();
    res = PQexecParams(conn, query, 3, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}

string productsIsEmpty() {
    string result;
    res = PQexec(conn, "SELECT SUM (count_all) "
                       "FROM products;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);
    char *count_all = nullptr;
    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        count_all = PQgetvalue(res, i, 0);
    }

    clearRes();
    return count_all;
}

string productsSalary() {
    string result;
    res = PQexec(conn,
                 "SELECT SUM (proceeds) * 0.1 FROM (SELECT products.price * products_users.count_buy as proceeds "
                 "FROM products_users "
                 "JOIN products ON products_users.product_id = products.id) as PR;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);
    char *count_all = nullptr;
    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        count_all = PQgetvalue(res, i, 0);
    }

    clearRes();
    return count_all;
}

string truncate() {
    string result;
    res = PQexec(conn,
                 "truncate table products_users cascade;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    clearRes();
    return "database_clear";
}


string selectMyOrder(const string &name) {
    string result;
    const char *query =
            "SELECT products.name, products_users.count_buy, products.price, products.price * products_users.count_buy "
            "FROM products_users "
            "JOIN users ON user_id = users.id "
            "JOIN products ON product_id = products.id "
            "WHERE users.name = $1;";
    const char *params_check[1];
    params_check[0] = name.c_str();
    res = PQexecParams(conn, query, 1, nullptr, params_check,
                       nullptr, nullptr, 0);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *price = PQgetvalue(res, i, 2);
        char *count_all = PQgetvalue(res, i, 3);
        result += "name: ";
        result += id;
        result += " count all: ";
        result += name;
        result += " Price: ";
        result += price;
        result += " price all: ";
        result += count_all;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string insertUser(const string &name) {
    const char *query =
            "INSERT INTO users (name) "
            " VALUES ($1);";
    const char *params[1];

    params[0] = name.c_str();
    res = PQexecParams(conn, query, 1, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}


string insertProducts(const string &name,
                      const string &price,
                      const string &count_all) {
    const char *query =
            "INSERT INTO products (name, price, count_all) "
            " VALUES ($1, $2, $3);";
    const char *params[3];

    params[0] = name.c_str();
    params[1] = price.c_str();
    params[2] = count_all.c_str();
    res = PQexecParams(conn, query, 3, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}